const express = require('express');

const app = express();
const port = process.env.PORT || 4000;

const data = [
  {
    name: 'LeBron James',
    team: 'Los Angeles Lakers',
    position: 'SF',
  },
  {
    name: 'Derrick Rose',
    team: 'Detroit Pistons',
    position: 'PG',
  },
  {
    name: 'Kawhi Leonard',
    team: 'Los Angeles Clippers',
    position: 'SF',
  }
];

app.get('/players', (req, res) => {
  res.status(200).json(data);
});

app.listen(port);

module.exports = app;
