const chai = require('chai');
const app = require('../../index');
const { describe, it } = require('mocha');
const { expect } = chai;
chai.use(require('chai-http'));

describe('Players', () => {
  it('Should return an array of the finest ballers', (done) => {
    chai.request(app)
      .get('/players')
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res.body).to.have.lengthOf(3);
        res.body.forEach((player) => {
          expect(player).to.have.property('name');
          expect(player).to.have.property('team');
          expect(player).to.have.property('position');
        });
        done();
      });
  });
});
